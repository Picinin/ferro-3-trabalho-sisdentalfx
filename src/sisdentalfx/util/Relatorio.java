/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisdentalfx.util;

import java.sql.ResultSet;
import javafx.embed.swing.SwingNode;
import javafx.scene.control.Alert;
import javax.swing.SwingUtilities;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JRViewer;
import sisdentalfx.db.util.Banco;
public class Relatorio 
{
    public static SwingNode gerarRelatorio(String sql,String relat)//dentro de um painel
    {  
        try
        {
            ResultSet rs = Banco.con.consultar(sql);
            JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);
            JasperPrint print = JasperFillManager.fillReport(relat, null, jrRS);
            JRViewer viewer = new JRViewer(print);
            viewer.setOpaque(true);
            viewer.setVisible(true);
            viewer.setZoomRatio(0.5f);
            viewer.setVisible(true);
            viewer.getHeight();
            SwingNode swingNode = new SwingNode();
            SwingUtilities.invokeLater(()->{swingNode.setContent(viewer);});
            return swingNode;
        } 
         catch (JRException erro)  
         {  
             Alert a = new Alert(Alert.AlertType.ERROR);
             a.setContentText(erro.getMessage());
             a.showAndWait();
             return null;
         }
    }    
}
