package sisdentalfx.ui;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import sisdentalfx.util.Relatorio;
/**
 * FXML Controller class
 *
 * @author Luish
 */
public class TelaRelatMateriaisController implements Initializable {

    @FXML
    private TextField txbusca;
    @FXML
    private ScrollPane spdado;
    @FXML
    private HBox dado;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
        SwingNode sn = Relatorio.gerarRelatorio("SELECT * FROM material ORDER BY material.mat_codigo ASC", "relatorios/MyReports/RelMaterial.jasper");
        dado.getChildren().clear();
        dado.getChildren().add(sn);
        
    }    

    @FXML
    private void evtBucscar(ActionEvent event) 
    {
        try
        {
            if(!txbusca.getText().isEmpty())
            {
                SwingNode sn = Relatorio.gerarRelatorio("SELECT * FROM material where upper(mat_descricao) LIKE upper('%"+txbusca.getText()+"%') ORDER BY material.mat_codigo ASC", "relatorios/MyReports/RelMaterial.jasper");
                dado.getChildren().clear();
                dado.getChildren().add(sn);
            }
            else
            {
                SwingNode sn = Relatorio.gerarRelatorio("SELECT * FROM material ORDER BY material.mat_codigo ASC", "relatorios/MyReports/RelMaterial.jasper");
                dado.getChildren().clear();
                dado.getChildren().add(sn);
            }
        }
        catch(Exception ex)
        {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("Não Há Dados Eminentes Nesta Consulta");
            a.showAndWait();
            txbusca.setText("");
            SwingNode sn = Relatorio.gerarRelatorio("SELECT * FROM material ORDER BY material.mat_codigo ASC", "relatorios/MyReports/RelMaterial.jasper");
            dado.getChildren().clear();
            dado.getChildren().add(sn);
        }
        
    }
    
}
