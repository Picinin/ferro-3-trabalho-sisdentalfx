/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisdentalfx.ui;

import java.net.URL;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.layout.HBox;
import sisdentalfx.util.Relatorio;

/**
 * FXML Controller class
 *
 * @author Luish
 */
public class TelaRelAgendaDoDiaController implements Initializable {

    @FXML
    private HBox dado;
    @FXML
    private DatePicker dtpicker;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
        setDataAtual();
    }    
    private void setDataAtual(){
        dtpicker.setValue(LocalDate.now());
        SwingNode sn = Relatorio.gerarRelatorio("select den_nome,pac_nome,con_horario,con_obs "
            + "from consulta inner join dentista on dentista.den_codigo = "
            + "consulta.den_codigo inner join paciente on paciente.pac_codigo = "
            + "consulta.pac_codigo where con_data = '"+dtpicker.getValue().toString()+"' order by dentista.den_nome,con_horario", "relatorios/MyReports/RelAgendaDoDia.jasper");
        dado.getChildren().clear();
        dado.getChildren().add(sn);
    }
    @FXML
    private void evtbusca(ActionEvent event) 
    {
        SwingNode sn;
        try{
            sn = Relatorio.gerarRelatorio("select den_nome,pac_nome,con_horario,con_obs "
                    + "from consulta inner join dentista on dentista.den_codigo = "
                    + "consulta.den_codigo inner join paciente on paciente.pac_codigo = "
                    + "consulta.pac_codigo where con_data = '"+dtpicker.getValue().toString()+"' order by dentista.den_nome,con_horario", "relatorios/MyReports/RelAgendaDoDia.jasper");
                     dado.getChildren().clear();
                     dado.getChildren().add(sn);
        }catch(Exception ex){
           try{
                setDataAtual();
           }catch(Exception exc){
               
           }
        }
    }
    
}
