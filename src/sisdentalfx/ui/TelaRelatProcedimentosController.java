/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisdentalfx.ui;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import sisdentalfx.util.Relatorio;

/**
 * FXML Controller class
 *
 * @author Luish
 */
public class TelaRelatProcedimentosController implements Initializable {

    @FXML
    private TextField txbusca;
    @FXML
    private ScrollPane spdado;
    @FXML
    private HBox dado;
    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
        SwingNode sn = Relatorio.gerarRelatorio("SELECT * FROM procedimentos order by pro_codigo asc", "relatorios/MyReports/RelProcedimento.jasper");
        dado.getChildren().clear();
        dado.getChildren().add(sn);
    }    

    @FXML
    private void evtBucscar(ActionEvent event) 
    {
        try
        {
            if(!txbusca.getText().isEmpty())
            {
                SwingNode sn = Relatorio.gerarRelatorio("SELECT * FROM procedimentos where upper(pro_descricao) LIKE upper('%"+txbusca.getText()+"%') ORDER BY procedimentos.pro_codigo asc", "relatorios/MyReports/RelProcedimento.jasper");
                dado.getChildren().clear();
                dado.getChildren().add(sn);
            }
            else
            {
                SwingNode sn = Relatorio.gerarRelatorio("SELECT * FROM procedimentos ORDER BY procedimentos.pro_codigo asc", "relatorios/MyReports/RelProcedimento.jasper");
                dado.getChildren().clear();
                dado.getChildren().add(sn);
            }
        }
        catch(Exception ex)
        {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("Não Há Dados Eminentes Nesta Consulta");
            txbusca.setText("");
            SwingNode sn = Relatorio.gerarRelatorio("SELECT * FROM procedimentos ORDER BY procedimentos.pro_codigo asc", "relatorios/MyReports/RelProcedimento.jasper");
            dado.getChildren().clear();
            dado.getChildren().add(sn);
            a.showAndWait();
        }
    }
    
}
